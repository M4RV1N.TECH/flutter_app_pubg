import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutterapppubg/screens/first.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const PrimaryColor = const Color(0xFF303030);

    return MaterialApp(
      theme: ThemeData(
        primaryColor: PrimaryColor,
      ),
      home: FirstRoute(),
    );
  }
}




