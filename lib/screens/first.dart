import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapppubg/screens/widgets/bottom_navigation_bar.dart';
import 'package:flutterapppubg/screens/second.dart';
import 'package:flutterapppubg/screens/widgets/group_image.dart';

class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: _buildAppBar(),
      body: _buildBody(context),
      bottomNavigationBar: WidgetBottomNavigationBar(),
    );
  }
}

Widget _buildAppBar() {
  return AppBar(
    leading: IconButton(
      icon: Icon(Icons.arrow_back_ios),
      onPressed: () {
        BuildContext context;
        Navigator.pop(context);
      },
    ),
    title: Align(
      alignment: Alignment.centerRight,
      // Align however you like (i.e .centerRight, centerLeft)
      child: Text("Катка"),
    ),
    actions: <Widget>[
      // action button
      ButtonBar(
        children: <Widget>[
          FlatButton(
            child: Text(
              'Редактировать',
              style: TextStyle(color: Colors.blue),
            ),
          ),
        ],
      ),
    ],
  );
}

Widget _buildBody(context) {
  return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        _groupImage(),
        _groupTiles(context),
      ],
    ),
  );
}

Padding _groupImage() {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 40.0),
    child: WidgetGroupImage(),
  );
}

Widget _groupTiles(context) {
  bool _switchVal = true;

  return Column(
    children: <Widget>[
      Container(
        color: Color(0xFF303030),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: Text('Пригласить в группу',
                  style: TextStyle(color: Colors.blue)),
              dense: true,
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SecondRoute()),
                );
              },
            ),
            Divider(
              color: Colors.grey,
              indent: 20,
            ),
            ListTile(
              title: Text('Добавить участников',
                  style: TextStyle(color: Colors.blue)),
              dense: true,
            ),
            Divider(
              color: Colors.grey,
              indent: 20,
            ),
            ListTile(
              title: Text('Уведомления', style: TextStyle(color: Colors.white)),
              dense: true,
              trailing: Switch(
                onChanged: (bool value) {
                  //var setState = setState(() => this.+switchVal = value);
                },
                value: _switchVal,
                activeTrackColor: Colors.blue,
                activeColor: Colors.white,
              ),
            ),
          ],
        ),
      ),
      Container(
        margin: const EdgeInsets.only(top: 40.0),
        color: Color(0xFF303030),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: Text('Админы', style: TextStyle(color: Colors.white)),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('1', style: TextStyle(color: Colors.grey)),
                  Icon(Icons.keyboard_arrow_right, color: Colors.grey),
                ],
              ),
              dense: true,
            ),
            Divider(
              color: Colors.grey,
              indent: 20,
            ),
            ListTile(
              title: Text('Участники', style: TextStyle(color: Colors.white)),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('3', style: TextStyle(color: Colors.grey)),
                  Icon(Icons.keyboard_arrow_right, color: Colors.grey),
                ],
              ),
              dense: true,
            ),
            Divider(
              color: Colors.grey,
              indent: 20,
            ),
            ListTile(
              title:
                  Text('Чёрный список', style: TextStyle(color: Colors.white)),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('0', style: TextStyle(color: Colors.grey)),
                  Icon(Icons.keyboard_arrow_right, color: Colors.grey),
                ],
              ),
              dense: true,
            ),
          ],
        ),
      ),
      Container(
        margin: const EdgeInsets.symmetric(vertical: 40.0),
        color: Color(0xFF303030),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: Text('Скрытый чат', style: TextStyle(color: Colors.white)),
              dense: true,
            ),
            Divider(
              color: Colors.grey,
              indent: 20,
            ),
            ListTile(
              title:
                  Text('Выйти из группы', style: TextStyle(color: Colors.red)),
              dense: true,
            ),
          ],
        ),
      ),
    ],
  );
}
