import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapppubg/screens/widgets/group_image.dart';
import 'package:flutter/services.dart';

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String groupLink = 'https:kdos.lofe/chat/fgjadijfg-fdidfg ldjkd ajgioje rgdkfvndkvjndfnv';
    const PrimaryColor = const Color(0xFF303030);

    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Text("Пригласить в группу"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              color: PrimaryColor,
              child: Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: WidgetGroupImage(),
                  ),
                  Expanded(
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Катка',
                            style: TextStyle(color: Colors.white, fontSize: 17.0,),
                          ),
                          SizedBox(height: 10.0),
                          Text(
                            groupLink,
                            style: TextStyle(color: Colors.blue),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                  'Любой пользователь kdos.life может подключиться к чату с помощью этой ссылки. Пошлите ссылку подругам или опубликуйте в записи, чтобы пригласить их в чат.',
                  style: TextStyle(color: Colors.white38)),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 20.0),
              color: PrimaryColor,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  CopyLinkTile(groupLink: groupLink),
                  Divider(
                    color: Colors.grey,
                    indent: 20,
                  ),
                  ListTile(
                    title: Text('Поделиться ссылкой',
                        style: TextStyle(color: Colors.white)),
                    dense: true,
                  ),
                ],
              ),
            ),
            Container(
              color: PrimaryColor,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    title: Text('Отключить приглашение в чат',
                        style: TextStyle(color: Colors.red)),
                    dense: true,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CopyLinkTile extends StatelessWidget {
  final String groupLink;
  CopyLinkTile({this.groupLink});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            backgroundColor: Colors.black,
            content: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  disabledColor: Colors.black,
                  onPressed: null,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Color(0xFF303030),
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    padding:
                        EdgeInsets.symmetric(horizontal: 40.0, vertical: 5.0),
                    child: Text(
                      'Готово',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white38,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
        Clipboard.setData(new ClipboardData(text: groupLink));
      },
      child: ListTile(
        title: Text('Копировать ссылку', style: TextStyle(color: Colors.white)),
        dense: true,
      ),
    );
  }
}
