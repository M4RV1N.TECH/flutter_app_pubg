import 'package:flutter/material.dart';

class WidgetBottomNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: 1,
      type: BottomNavigationBarType.fixed,
      backgroundColor: const Color(0xFF303030),
      selectedItemColor: Colors.blue,
      unselectedItemColor: Colors.grey,
      selectedFontSize: 10,
      unselectedFontSize: 10,

      items: [
        BottomNavigationBarItem(
          icon: new Icon(Icons.home),
          title: new Text('Лента'),
        ),
        BottomNavigationBarItem(
          icon: new Icon(Icons.chat),
          title: new Text('Чаты'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.my_location),
          title: Text('Рядом'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.notifications_active),
          title: Text('Уведомления'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          title: Text('Профиль'),
        ),
      ],
    );
  }
}