import 'package:flutter/material.dart';

class WidgetGroupImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 35,
      backgroundColor: Colors.white,
      child: CircleAvatar(
        radius: 33,
        backgroundColor: Color(0xFF303030),
        foregroundColor: Colors.white,
        child: Icon(
          Icons.people,
          size: 35.0,
        ),
      ),
    );
  }
}